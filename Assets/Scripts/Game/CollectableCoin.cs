﻿using Game.Inventory;
using UnityEngine;
using Zenject;

namespace Game {

    [RequireComponent(typeof(AudioSource))]
    public class CollectableCoin : MonoBehaviour, IInteractiveObject {
        [SerializeField] private int _amount;

        private InventoryManager _inventoryManager;
        private AudioSource _audioSource;

        [Inject]
        public void Init(InventoryManager inventoryManager) {
            _inventoryManager = inventoryManager;
        }

        private void Awake() {
            _audioSource = GetComponent<AudioSource>();
        }

        public string ActionName {
            get {
                if (_amount == 1) {
                    return "Collect (Coin)";
                }
                return string.Format("Collect (Coin x{0})", _amount);
            }
        }

        public bool ActionAvailable {
            get { return true; }
        }

        public void Interact() {
            _inventoryManager.Coins += _amount;

            _audioSource.Play();

            var disappearAnimation = GetComponent<DisappearAnimation>();
            if (disappearAnimation == null) {
                Destroy(gameObject);
            } else {
                Destroy(this);
                disappearAnimation.Disappear();
            }
        }
    }
}