﻿using Game.Inventory;
using UnityEngine;
using Zenject;

namespace Game {

    [RequireComponent(typeof(AudioSource))]
    public class CollectableItem : MonoBehaviour, IInteractiveObject {
        [SerializeField] private int _itemId;
        [SerializeField] public int _count;

        private InventoryManager _inventoryManager;
        private ItemsDic _itemsDic;
        private string _itemName;
        private AudioSource _audioSource;

        [Inject]
        public void Init(InventoryManager inventoryManager, ItemsDic itemsDic) {
            _inventoryManager = inventoryManager;
            _itemsDic = itemsDic;
        }

        private void Awake() {
            _itemName = _itemsDic.FindInfo(_itemId).Name;
            _audioSource = GetComponent<AudioSource>();
        }

        public int Count {
            set { _count = value; }
        }

        public string ActionName {
            get {
                if (_count == 1) {
                    return string.Format("Collect ({0})", _itemName);
                }
                return string.Format("Collect ({0} x{1})", _itemName, _count);
            }
        }

        public bool ActionAvailable {
            get { return true; }
        }

        public void Interact() {
            var collected = _inventoryManager.TryAddItem(_itemId, _count);
            if (collected) {
                _audioSource.Play();

                var disappearAnimation = GetComponent<DisappearAnimation>();
                if (disappearAnimation == null) {
                    Destroy(gameObject);
                } else {
                    Destroy(this);
                    disappearAnimation.Disappear();
                }
            }
        }
    }
}