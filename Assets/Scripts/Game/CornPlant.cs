﻿using System.Collections;
using Game.Inventory;
using UnityEngine;

namespace Game {
    [RequireComponent(typeof(AudioSource))]
    public class CornPlant : MonoBehaviour, IDestroyable, IPlant {
        [SerializeField] private float _timeToGrow = 30f;
        [SerializeField] private Transform[] _corns;
        [SerializeField] private AnimationCurve _mainGrowindCurve;
        [SerializeField] private AnimationCurve _cornGrowindCurve;
        [SerializeField] private ParticleSystem _particleSystem;
        [SerializeField] private Transform _modelContainer;
        [SerializeField] private DropSpawner _dropSpawner;

        private bool _isFullyGrown;
        private int _fertilizer;
        private AudioSource _audioSource;

        public SmallPlot ParentPlot { private get; set; }

        private void Awake() {
            _audioSource = GetComponent<AudioSource>();
        }

        public void ResetPosition(Transform parent) {
            var angle = Random.value * 360f;

            transform.parent = parent;
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.Euler(0, 0, angle);
            transform.localScale = Vector3.one;
        }

        public void StartGrowingAnimation() {
            _audioSource.Play();

            transform.localScale = Vector3.one * 0.2f;
            StartCoroutine(GrowCoroutine(_timeToGrow));
        }

        public void Fertilize() {
            _fertilizer++;
        }


        private IEnumerator GrowCoroutine(float duration) {
            var timer = 0f;
            while (timer < duration) {
                timer += Time.deltaTime;

                while (_fertilizer > 0) {
                    _fertilizer--;
                    timer += 1;
                }

                var mainProgress = _mainGrowindCurve.Evaluate(timer / _timeToGrow);
                var size = Mathf.LerpUnclamped(0.2f, 1f, mainProgress);
                transform.localScale = Vector3.one * size;

                var cornProgress = _cornGrowindCurve.Evaluate(timer / _timeToGrow);
                size = Mathf.LerpUnclamped(0f, 1f, cornProgress);
                foreach (var corn in _corns) {
                    corn.transform.localScale = Vector3.one * size;
                }
                yield return null;
            }

            _isFullyGrown = true;
        }

        public void TakeDamage(int damage) {
            if (!_isFullyGrown) {
                return;
            }

            ParentPlot.Clear();
            _modelContainer.gameObject.SetActive(false);

            GetComponent<CapsuleCollider>().enabled = false;

            var berries = Random.Range(2, 4);
            for (var i = 0; i < berries; i++) {
                _dropSpawner.SpawnDrop(ItemsDic.IdCorn, 1);
            }

            var seeds = Random.Range(1, 3);
            for (var i = 0; i < seeds; i++) {
                _dropSpawner.SpawnDrop(ItemsDic.IdCornSeed, 1);
            }

            _particleSystem.Play();
            Destroy(gameObject, 3);
        }
    }
}