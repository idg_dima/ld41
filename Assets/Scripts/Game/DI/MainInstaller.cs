using System.Collections.Generic;
using Game.Inventory;
using Game.Tools;
using Zenject;

namespace Game.DI {
    public class MainInstaller : MonoInstaller<MainInstaller>
    {
        public override void InstallBindings() {
            Container.Bind<GameStateManager>().FromNewComponentOn(gameObject).AsSingle().NonLazy();

            Container.Bind<InventoryManager>().AsSingle().NonLazy();

            Container.Bind<ItemsDic>().FromResource("ItemsDic");
            Container.Bind<CoinsDic>().FromResource("CoinsDic");

            Container.Bind<SeedGun>().AsSingle().NonLazy();
            Container.Bind<Katana>().AsSingle().NonLazy();
            Container.Bind<Shotgun>().AsSingle().NonLazy();

            var toolsById = new Dictionary<int, ITool> {
                {ItemsDic.IdSeedGun, Container.Resolve<SeedGun>()},
                {ItemsDic.IdKatana, Container.Resolve<Katana>()},
                {ItemsDic.IdShotgun, Container.Resolve<Shotgun>()},
            };
            Container.Bind<Dictionary<int, ITool>>().FromInstance(toolsById);

            PrefabsInstaller.Install(Container);
        }
    }
}