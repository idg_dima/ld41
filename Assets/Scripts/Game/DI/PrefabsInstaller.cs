﻿using System.Collections.Generic;
using Game.Inventory;
using Game.Inventory.Gui;
using Jir.Prefabs;
using Zenject;

namespace Game.DI {
    public class PrefabsInstaller : Installer<PrefabsInstaller> {
        private const string PrefabsFolder = "Prefabs/";

        public override void InstallBindings() {
            BindPrefab<InventorySlot>("InventorySlot");

            var instantiator = Container.Resolve<IInstantiator>();
            var plantPrefabsBySeedId = new Dictionary<int, Prefab<IPlant>> {
                {ItemsDic.IdBerriesSeed, new Prefab<IPlant>(instantiator, PrefabsFolder + "BerriesPlant")},
                {ItemsDic.IdCornSeed, new Prefab<IPlant>(instantiator, PrefabsFolder + "CornPlant")},
            };
            Container.Bind<Dictionary<int, Prefab<IPlant>>>().FromInstance(plantPrefabsBySeedId);
        }

        private ScopeArgConditionCopyNonLazyBinder BindPrefab<T>(string path) {
            return Container.Bind<Prefab<T>>().FromMethod(context => CreatePrefab<T>(context, PrefabsFolder + path));
        }

        private ScopeArgConditionCopyNonLazyBinder BindPrefab<T>(string path, string id) {
            return Container.Bind<Prefab<T>>().WithId(id).FromMethod(context => CreatePrefab<T>(context, PrefabsFolder + path));
        }

        private static Prefab<T> CreatePrefab<T>(InjectContext injectContext, string path) {
            var instantiator = injectContext.Container.Resolve<IInstantiator>();
            return new Prefab<T>(instantiator, path);
        }
    }
}