﻿using System.Collections;
using UnityEngine;

namespace Game {
    public class DisappearAnimation : MonoBehaviour {

        [SerializeField] private AnimationCurve _scaleCurve;
        [SerializeField] private float _duration;
        [SerializeField] private float _delay;

        public void Disappear() {
            var rigidBody = GetComponent<Rigidbody>();
            if (rigidBody != null) {
                DestroyImmediate(rigidBody);
            }

            StartCoroutine(AnimationCoroutine());
        }

        private IEnumerator AnimationCoroutine() {
            var startScale = transform.localScale;
            var timer = 0f;
            while (timer < _duration) {
                timer += Time.deltaTime;

                var progress = _scaleCurve.Evaluate(timer / _duration);
                transform.localScale = Vector3.LerpUnclamped(startScale, Vector3.zero, progress);
                yield return null;
            }

            while (timer < _delay) {
                timer += Time.deltaTime;
                yield return null;
            }
            Destroy(gameObject);
        }
    }
}