﻿using Game.Inventory;
using UnityEngine;
using Zenject;

namespace Game {
    public class DropSpawner : MonoBehaviour {
        private ItemsDic _itemsDic;
        private IInstantiator _instantiator;

        [Inject]
        public void Init(ItemsDic itemsDic, IInstantiator instantiator) {
            _itemsDic = itemsDic;
            _instantiator = instantiator;
        }

        public void SpawnDrop(int itemId, int count) {
            var itemInfo = _itemsDic.FindInfo(itemId);
            var prefab = itemInfo.Prefab;

            var drop = _instantiator.InstantiatePrefab(prefab);
            var positionOffset = Random.insideUnitSphere * 0.1f;
            drop.transform.position = transform.position + positionOffset;
            drop.transform.rotation = Random.rotationUniform;

            var collectableItem = drop.GetComponent<CollectableItem>();
            collectableItem.Count = count;

            var rigidBody = drop.GetComponent<Rigidbody>();
            rigidBody.AddForce(Vector3.up * 100 * rigidBody.mass);
        }
    }
}