﻿using Game.Inventory;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game {
    public class GameGui : MonoBehaviour {
        [SerializeField] private Text _actionText;
        [SerializeField] private Text _coinsText;

        private InventoryManager _inventoryManager;

        [Inject]
        public void Init(InventoryManager inventoryManager) {
            _inventoryManager = inventoryManager;
        }

        private void Awake() {
            _inventoryManager.CoinsChangedEvent += UpdateCoins;
            UpdateCoins();
        }

        public IInteractiveObject TargetObject {
            set {
                if (value == null) {
                    _actionText.text = "";
                    return;
                }

                var prefix = value.ActionAvailable ? "[E] " : "";
                _actionText.text = prefix + value.ActionName;
            }
        }

        private void UpdateCoins() {
            _coinsText.text = _inventoryManager.Coins.ToString();
        }
    }
}