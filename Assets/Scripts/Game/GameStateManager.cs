﻿using Game.Inventory.Gui;
using UnityEngine;
using Zenject;

namespace Game {
    public class GameStateManager : MonoBehaviour {

        private InventoryGui _inventoryGui;
        private GameGui _gameGui;
        private Player _player;
        private bool _inventoryActive;

        [Inject]
        public void Init(InventoryGui inventoryGui, GameGui gameGui, Player player) {
            _inventoryGui = inventoryGui;
            _gameGui = gameGui;
            _player = player;
        }

        private void Start() {
            InventoryActive = false;
        }

        private bool InventoryActive {
            get { return _inventoryActive; }
            set
            {
                _inventoryActive = value;
                _inventoryGui.Visible = _inventoryActive;
                _player.MovementActive = !_inventoryActive;
                _player.enabled = !_inventoryActive;
                Cursor.visible = _inventoryActive;

                if (value) {
                    _gameGui.TargetObject = null;
                }
            }
        }

        private void Update() {
            if (Input.GetKeyDown(KeyCode.Tab) || Input.GetKeyDown(KeyCode.I)) {
                InventoryActive = !InventoryActive;
            }
        }
    }
}