﻿namespace Game {
    public interface IDestroyable {
        void TakeDamage(int damage);
    }
}