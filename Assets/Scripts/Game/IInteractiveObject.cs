﻿namespace Game {
    public interface IInteractiveObject {
        string ActionName { get; }

        bool ActionAvailable { get; }

        void Interact();
    }
}