﻿using UnityEngine;

namespace Game {
    public interface IPlant {
        void ResetPosition(Transform parent);

        void StartGrowingAnimation();

        void Fertilize();

        SmallPlot ParentPlot { set; }
    }
}