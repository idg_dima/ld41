﻿namespace Game {
    public interface IPlot {

        void PlantSeed(int seedId);

        void Fertilize();

    }
}