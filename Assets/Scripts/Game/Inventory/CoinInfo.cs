﻿using System;
using UnityEngine;

namespace Game.Inventory {
    [Serializable]
    public class CoinInfo {
        public int Amount;
        public GameObject Prefab;
    }
}