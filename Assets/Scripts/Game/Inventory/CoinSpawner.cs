﻿using System.Collections;
using UnityEngine;
using Zenject;

namespace Game.Inventory {

    [RequireComponent(typeof(AudioSource))]
    public class CoinSpawner : MonoBehaviour {
        private const float CoinsDelay = 0.2f;

        private IInstantiator _instantiator;
        private CoinsDic _coinsDic;
        private AudioSource _audioSource;

        [Inject]
        public void Init(IInstantiator instantiator, CoinsDic coinsDic) {
            _instantiator = instantiator;
            _coinsDic = coinsDic;
        }

        private void Awake() {
            _audioSource = GetComponent<AudioSource>();
        }

        public void SpawnCoins(int amount) {
            StartCoroutine(SpawnCoinsCoroutine(amount));
        }

        private IEnumerator SpawnCoinsCoroutine(int amount) {
            while (amount != 0) {

                var coinInfo = _coinsDic.FindBiggestCoin(amount);
                if (coinInfo == null) {
                    yield break;
                }

                amount -= coinInfo.Amount;
                SpawnCoin(coinInfo);

                if (amount != 0) {
                    yield return new WaitForSeconds(CoinsDelay);
                }
            }
        }

        private void SpawnCoin(CoinInfo coinInfo) {
            _audioSource.Play();

            var coin = _instantiator.InstantiatePrefab(coinInfo.Prefab);
            coin.transform.position = transform.position;

            var rigidBody = coin.GetComponent<Rigidbody>();
            var force = Mathf.Lerp(150, 400, Random.value) * rigidBody.mass;
            rigidBody.AddForce(transform.forward * force);
            rigidBody.AddTorque(Random.insideUnitSphere * 15);
        }
    }
}