﻿using System.Linq;
using UnityEngine;

namespace Game.Inventory {

    [CreateAssetMenu(fileName = "CoinsDic", menuName = "Dictionaries/Coins", order = 1)]
    public class CoinsDic : ScriptableObject {
        public CoinInfo[] CoinsInfo;

        public CoinInfo FindBiggestCoin(int amount) {
            return CoinsInfo.LastOrDefault(c => c.Amount <= amount);
        }
    }
}