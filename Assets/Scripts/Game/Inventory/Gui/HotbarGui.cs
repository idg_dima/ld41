﻿using Jir.Prefabs;
using UnityEngine;
using Zenject;

namespace Game.Inventory.Gui {
    public class HotbarGui : MonoBehaviour {
        private Prefab<InventorySlot> _slotPrefab;
        private InventoryManager _inventoryManager;
        private ItemsDic _itemsDic;

        private InventorySlot[] _slots;

        [Inject]
        public void Init(
            Prefab<InventorySlot> slotPrefab, InventoryManager inventoryManager, ItemsDic itemsDic) {

            _slotPrefab = slotPrefab;
            _inventoryManager = inventoryManager;
            _itemsDic = itemsDic;

            _inventoryManager.InventoryChangedEvent += UpdateSlots;
        }

        private void Awake() {
            _slots = new InventorySlot[InventoryManager.HotbarSlotsCount];
            for (var i = 0; i < _slots.Length; i++) {
                var slot = _slotPrefab.Instantiate();
                slot.transform.SetParent(transform, false);
                slot.SlotIndex = i;
                slot.IsHotbar = true;
                slot.TransparentBg = true;
                _slots[i] = slot;
            }

            UpdateSlots();
        }

        private void Update() {
            for (var i = 0; i < InventoryManager.HotbarSlotsCount; i++) {
                var keyCode = KeyCode.Alpha1 + i;
                if (Input.GetKeyDown(keyCode)) {
                    _inventoryManager.SelectedHotbarSlotIndex = i;
                    return;
                }
            }

            if (!Mathf.Approximately(Input.mouseScrollDelta.y, 0)) {
                var amount = Mathf.FloorToInt(Mathf.Abs(Input.mouseScrollDelta.y));
                var sign = (int) Mathf.Sign(Input.mouseScrollDelta.y);
                _inventoryManager.SelectedHotbarSlotIndex -= amount * sign;
            }
        }

        private void UpdateSlots() {
            var items = _inventoryManager.HotbarItems;
            for (var i = 0; i < _slots.Length; i++) {
                _slots[i].Item = items[i];
                _slots[i].TransparentBg = i != _inventoryManager.SelectedHotbarSlotIndex;
            }
        }
    }
}