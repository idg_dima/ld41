﻿using Jir.Prefabs;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.Inventory.Gui {
    public class InventoryGui : MonoBehaviour {
        [SerializeField] private RectTransform _slotsContainer;
        [SerializeField] private RectTransform _followingMouse;
        [SerializeField] private Image _draggingImage;
        [SerializeField] private Text _itemNameText;
        [SerializeField] private RectTransform _canvasTransform;
        [SerializeField] private Text _inventoryHintText;

        private InventorySlot _currentSlot;

        private Prefab<InventorySlot> _slotPrefab;
        private InventoryManager _inventoryManager;
        private ItemsDic _itemsDic;

        private InventorySlot[] _slots;
        private InventorySlot _draggingSlot;

        [Inject]
        public void Init(
            Prefab<InventorySlot> slotPrefab,
            InventoryManager inventoryManager,
            ItemsDic itemsDic,
            HotbarGui hotbarGui) {

            _slotPrefab = slotPrefab;
            _inventoryManager = inventoryManager;
            _itemsDic = itemsDic;
        }

        private void Awake() {
            _slots = new InventorySlot[InventoryManager.InventorySlotsCount];
            for (var i = 0; i < _slots.Length; i++) {
                var slot = _slotPrefab.Instantiate();
                slot.transform.SetParent(_slotsContainer, false);
                slot.SlotIndex = i;
                _slots[i] = slot;
            }
        }

        public bool Visible {
            set {
                gameObject.SetActive(value);
                if (value) {
                    UpdateSlots();
                    _inventoryHintText.gameObject.SetActive(false);
                } else {
                    _draggingSlot = null;
                    _itemNameText.text = "";
                }
            }
        }

        public InventorySlot CurrentSlot {
            get { return _currentSlot; }
            set {
                _currentSlot = value;
                UpdateItemName();
            }
        }

        private void UpdateItemName() {
            if (_draggingSlot == null && _currentSlot != null && _currentSlot.Item != null) {
                var itemInfo = _itemsDic.FindInfo(_currentSlot.Item.itemId);
                _itemNameText.text = itemInfo.Name;
            } else {
                _itemNameText.text = "";
            }
        }

        private void UpdateSlots() {
            var items = _inventoryManager.InventoryItems;
            for (var i = 0; i < _slots.Length; i++) {
                _slots[i].Item = items[i];
            }
        }

        private void Update() {
            if (_draggingSlot == null && CurrentSlot != null && Input.GetMouseButtonDown(0)) {
                StartDragging(CurrentSlot);
            }

            if (_draggingSlot != null && Input.GetMouseButtonUp(0)) {
                StopDragging(CurrentSlot);
            }

            var screenPoint = Input.mousePosition;
            var position = Camera.main.ScreenToViewportPoint(screenPoint);
            position.x *= _canvasTransform.sizeDelta.x;
            position.y = _canvasTransform.sizeDelta.y * position.y - Screen.height;

            _followingMouse.anchoredPosition = position;
        }

        public void StartDragging(InventorySlot slot) {
            if (slot.Item == null) {
                return;
            }

            _draggingSlot = slot;
            _draggingImage.gameObject.SetActive(true);
            _draggingImage.sprite = _itemsDic.FindInfo(slot.Item.itemId).InventorySprite;
            _itemNameText.text = "";
        }

        public void StopDragging(InventorySlot slot) {
            if (_draggingSlot == null || slot != null && _draggingSlot.Item == slot.Item) {
                _draggingImage.gameObject.SetActive(false);
                _draggingSlot = null;
                UpdateItemName();
                return;
            }

            if (slot != null) {
                if (!_draggingSlot.IsHotbar && !slot.IsHotbar) {
                    _inventoryManager.SwapInventoryItems(_draggingSlot.SlotIndex, slot.SlotIndex);
                    UpdateSlots();
                } else {
                    if (_draggingSlot.IsHotbar && !slot.IsHotbar) {
                        _inventoryManager.RemoveFromHotbar(_draggingSlot.SlotIndex);
                    }

                    if (!_draggingSlot.IsHotbar && slot.IsHotbar) {
                        _inventoryManager.MoveToHotbar(_draggingSlot.SlotIndex, slot.SlotIndex);
                    }

                    if (_draggingSlot.IsHotbar && slot.IsHotbar) {
                        _inventoryManager.SwapHotbarItems(_draggingSlot.SlotIndex, slot.SlotIndex);
                    }
                }
            }

            _draggingImage.gameObject.SetActive(false);
            _draggingSlot = null;
            UpdateItemName();
        }
    }
}