﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.Inventory.Gui {
    public class InventorySlot : MonoBehaviour {
        [SerializeField] private Image _bgImage;
        [SerializeField] private Image _itemImage;
        [SerializeField] private Text _counterText;

        public int SlotIndex;
        public bool IsHotbar;

        private ItemsDic _itemsDic;
        private InventoryItemInfo _item;
        private InventoryGui _inventoryGui;
        private InventorySlot[] _slots;

        [Inject]
        public void Init(ItemsDic itemsDic, InventoryGui inventoryGui) {
            _itemsDic = itemsDic;
            _inventoryGui = inventoryGui;
        }

        private void Awake() {
            _itemImage.gameObject.SetActive(false);
        }

        public InventoryItemInfo Item {
            get { return _item; }
            set {
                if (_item != value) {
                    _item = value;
                    _itemImage.gameObject.SetActive(value != null);
                    _itemImage.sprite = value == null ? null : _itemsDic.FindInfo(value.itemId).InventorySprite;
                }
                _counterText.text = value == null ? "" : value.count.ToString();
            }
        }

        public bool TransparentBg {
            set {
                _bgImage.color = new Color(1, 1, 1, value ? 0.25f : 1);
            }
        }

        [UsedImplicitly]
        public void OnPointerEnter() {
            _inventoryGui.CurrentSlot = this;
        }

        [UsedImplicitly]
        public void OnPointerExit() {
            _inventoryGui.CurrentSlot = null;
        }
    }
}