﻿namespace Game.Inventory {
    public class InventoryItemInfo {
        public int itemId;
        public int count;

        public InventoryItemInfo(int itemId, int count) {
            this.itemId = itemId;
            this.count = count;
        }
    }
}