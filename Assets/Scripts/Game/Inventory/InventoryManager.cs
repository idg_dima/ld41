﻿using System;
using UnityEngine;

namespace Game.Inventory {
    public class InventoryManager {
        public const int InventorySlotsCount = 20;
        public const int HotbarSlotsCount = 5;

        public InventoryItemInfo[] InventoryItems { get; private set; }
        public InventoryItemInfo[] HotbarItems { get; private set; }

        public event Action InventoryChangedEvent = () => {};
        public event Action CoinsChangedEvent = () => {};

        private int _selectedHotbarSlotIndex;
        private int _coins;

        public InventoryManager() {
            InventoryItems = new InventoryItemInfo[InventorySlotsCount];
            HotbarItems = new InventoryItemInfo[HotbarSlotsCount];
        }

        public bool HasItem(int id, int count = 1) {
            var index = FindItemIndex(InventoryItems, id);
            if (index == null) {
                return false;
            }

            var item = InventoryItems[index.Value];
            if (item.count < count) {
                return false;
            }

            return true;
        }

        public bool TryConsume(int id, int count = 1) {
            var index = FindItemIndex(InventoryItems, id);
            if (index == null) {
                return false;
            }

            var item = InventoryItems[index.Value];
            if (item.count < count) {
                return false;
            }
            item.count -= count;
            if (item.count == 0) {
                InventoryItems[index.Value] = null;

                var hotbarIndex = FindItemIndex(HotbarItems, id);
                if (hotbarIndex != null) {
                    HotbarItems[hotbarIndex.Value] = null;
                }
            }
            InventoryChangedEvent();
            return true;
        }

        private int? FindItemIndex(InventoryItemInfo[] items, int id) {
            for (var i = 0; i < items.Length; i++) {
                var item = items[i];
                if (item != null && item.itemId == id) {
                    return i;
                }
            }
            return null;
        }

        private int? FindEmptySlotIndex(InventoryItemInfo[] items) {
            for (var i = 0; i < items.Length; i++) {
                var item = items[i];
                if (item == null) {
                    return i;
                }
            }
            return null;
        }

        public bool TryAddItem(int id, int count) {
            var index = FindItemIndex(InventoryItems, id);
            if (index != null) {
                InventoryItems[index.Value].count += count;
                InventoryChangedEvent();
                return true;
            }

            var emptySlotIndex = FindEmptySlotIndex(InventoryItems);
            if (emptySlotIndex == null) {
                return false;
            }

            var itemInfo = new InventoryItemInfo(id, count);
            InventoryItems[emptySlotIndex.Value] = itemInfo;

            var emptyHotbarIndex = FindEmptySlotIndex(HotbarItems);
            if (emptyHotbarIndex != null) {
                HotbarItems[emptyHotbarIndex.Value] = itemInfo;
            }
            InventoryChangedEvent();
            return true;
        }

        public void SwapInventoryItems(int slotIndex1, int slotIndex2) {
            SwapItems(InventoryItems, slotIndex1, slotIndex2);
            InventoryChangedEvent();
        }

        public void SwapHotbarItems(int slotIndex1, int slotIndex2) {
            SwapItems(HotbarItems, slotIndex1, slotIndex2);
            InventoryChangedEvent();
        }

        private void SwapItems(InventoryItemInfo[] items, int slotIndex1, int slotIndex2) {
            var tmp = items[slotIndex1];
            items[slotIndex1] = items[slotIndex2];
            items[slotIndex2] = tmp;
        }

        public void MoveToHotbar(int slotIndex, int hotbarIndex) {
            var item = InventoryItems[slotIndex];
            var oldIndex = FindItemIndex(HotbarItems, item.itemId);
            if (oldIndex != null) {
                HotbarItems[oldIndex.Value] = null;
            }

            HotbarItems[hotbarIndex] = item;
            InventoryChangedEvent();
        }

        public void RemoveFromHotbar(int hotbarIndex) {
            HotbarItems[hotbarIndex] = null;
            InventoryChangedEvent();
        }

        public int SelectedHotbarSlotIndex {
            get { return _selectedHotbarSlotIndex; }
            set {
                _selectedHotbarSlotIndex = Mathf.Clamp(value, 0, HotbarSlotsCount - 1);
                InventoryChangedEvent();
            }
        }

        public InventoryItemInfo SelectedItem {
            get { return HotbarItems[_selectedHotbarSlotIndex]; }
        }

        public int Coins {
            get { return _coins; }
            set {
                _coins = value;
                CoinsChangedEvent();
            }
        }
    }
}