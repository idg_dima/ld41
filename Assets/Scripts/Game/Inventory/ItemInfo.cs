﻿using System;
using UnityEngine;

namespace Game.Inventory {

    [Serializable]
    public class ItemInfo {
        public int Id;
        public string Name;
        public int Price;
        public Sprite InventorySprite;
        public GameObject Prefab;
        public GameObject EquippedPrefab;
    }
}