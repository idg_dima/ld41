﻿using System.Linq;
using UnityEngine;

namespace Game.Inventory {

    [CreateAssetMenu(fileName = "ItemsDic", menuName = "Dictionaries/Items", order = 1)]
    public class ItemsDic : ScriptableObject {
        public const int IdSeedGun = 1;
        public const int IdBerriesSeed = 2;
        public const int IdBerry = 3;
        public const int IdKatana = 4;
        public const int IdCorn = 5;
        public const int IdCornSeed = 6;
        public const int IdShotgun = 7;

        public ItemInfo[] ItemsInfo;

        public ItemInfo FindInfo(int id) {
            return ItemsInfo.FirstOrDefault(i => i.Id == id);
        }
    }
}