﻿using Game.Inventory;
using UnityEngine;

namespace Game {

    [RequireComponent(typeof(AudioSource))]
    public class Picture : MonoBehaviour, IDestroyable {
        [SerializeField] private int _health;
        [SerializeField] private int _coins;
        [SerializeField] private Vector3 _correctAngles;
        [SerializeField] private CoinSpawner _coinSpawner;

        private AudioSource _audioSource;

        private void Awake() {
            _audioSource = GetComponent<AudioSource>();
        }

        public void TakeDamage(int damage) {
            if (_health == 0) {
                return;
            }

            _health = Mathf.Max(_health - damage, 0);
            if (_health == 0) {
                _audioSource.Play();
                transform.rotation = Quaternion.Euler(_correctAngles);
                _coinSpawner.SpawnCoins(_coins);
            }
        }
    }
}