﻿using System.Collections.Generic;
using Game.Inventory;
using Game.Tools;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using Zenject;

namespace Game {

    [RequireComponent(typeof(FirstPersonController))]
    public class Player : MonoBehaviour {
        private const float InteractDistance = 3f;

        [SerializeField] private Transform _equippedItemContainer;

        private Camera _camera;
        private FirstPersonController _fpsController;
        private GameGui _gameGui;
        private Dictionary<int, ITool> _toolsById;
        private InventoryManager _inventoryManager;
        private ItemsDic _itemsDic;
        private IInstantiator _instantiator;

        private InventoryItemInfo _currentItem;
        private GameObject _equippedItem;
        private int _interactiveLayerMask;

        [Inject]
        public void Init(
            Camera camera,
            GameGui gameGui,
            Dictionary<int, ITool> toolById,
            InventoryManager inventoryManager,
            ItemsDic itemsDic,
            IInstantiator instantiator) {

            _camera = camera;
            _gameGui = gameGui;
            _toolsById = toolById;
            _inventoryManager = inventoryManager;
            _itemsDic = itemsDic;
            _instantiator = instantiator;
        }

        private void Awake() {
            _fpsController = GetComponent<FirstPersonController>();

            _inventoryManager.InventoryChangedEvent += OnInventoryChanged;
            _interactiveLayerMask = LayerMask.GetMask("Interactive");
        }

        private void Update() {
            RaycastHit hitInfo;
            var ray = new Ray(_camera.transform.position, _camera.transform.forward);
            var hit = Physics.Raycast(ray, out hitInfo, 200, _interactiveLayerMask);

            if (_currentItem != null && _toolsById.ContainsKey(_currentItem.itemId)) {
                var tool = _toolsById[_currentItem.itemId];
                if (tool != null) {
                    if (Input.GetMouseButtonDown(0)) {
                        tool.Use(_equippedItem, hit, hitInfo);
                    } else if (Input.GetMouseButtonDown(1)) {
                        tool.ChangeMode(_equippedItem);
                    }
                }
            }

            IInteractiveObject interactTarget = null;
            if (hit && hitInfo.distance <= InteractDistance) {
                interactTarget = hitInfo.collider.GetComponent<IInteractiveObject>();
            }
            _gameGui.TargetObject = interactTarget;

            if (interactTarget != null && Input.GetKeyDown(KeyCode.E)) {
                interactTarget.Interact();
            }
        }

        public bool MovementActive {
            set {
                _fpsController.enabled = value;
                _fpsController.CursorLocked = value;
            }
        }

        private void OnInventoryChanged() {
            if (_currentItem == _inventoryManager.SelectedItem) {
                return;
            }

            _currentItem = _inventoryManager.SelectedItem;
            if (_equippedItem) {
                Destroy(_equippedItem);
            }

            if (_currentItem == null) {
                return;
            }
            var itemInfo = _itemsDic.FindInfo(_currentItem.itemId);
            if (itemInfo.EquippedPrefab == null) {
                return;
            }

            _equippedItem = _instantiator.InstantiatePrefab(itemInfo.EquippedPrefab, _equippedItemContainer);
        }
    }
}