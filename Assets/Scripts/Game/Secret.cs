﻿using Game.Inventory;
using UnityEngine;

namespace Game {
    public class Secret : MonoBehaviour, IDestroyable {
        [SerializeField] private int _health;
        [SerializeField] private int _coinsToSpawn;
        [SerializeField] private CoinSpawner _coinsSpawner;

        public void TakeDamage(int damage) {
            if (_health <= 0) {
                return;
            }

            _health -= damage;
            if (_health == 0) {
                _coinsSpawner.SpawnCoins(_coinsToSpawn);
            }
        }
    }
}