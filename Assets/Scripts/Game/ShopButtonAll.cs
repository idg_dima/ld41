﻿using System.Collections;
using Game.Inventory;
using UnityEngine;
using Zenject;

namespace Game {

    [RequireComponent(typeof(AudioSource))]
    public class ShopButtonAll : MonoBehaviour, IInteractiveObject {

        [SerializeField] private CoinSpawner _coinSpawner;

        private InventoryManager _inventoryManager;
        private ItemsDic _itemsDic;
        private AudioSource _audioSource;

        [Inject]
        public void Init(InventoryManager inventoryManager, ItemsDic itemsDic) {
            _inventoryManager = inventoryManager;
            _itemsDic = itemsDic;
        }

        private void Awake() {
            _audioSource = GetComponent<AudioSource>();
        }

        public string ActionName {
            get {
                var item = _inventoryManager.SelectedItem;
                if (item == null) {
                    return "Select items in hotbar to sell them";
                }

                var itemInfo = _itemsDic.FindInfo(item.itemId);
                if (itemInfo.Price == 0) {
                    return string.Format("{0} can not be sold", itemInfo.Name);
                }

                var price = itemInfo.Price * item.count;
                var priceText = price + (price == 1 ? " coin" : " coins");
                return string.Format("Sell {0} x{1} for {2}", itemInfo.Name, item.count, priceText);
            }
        }

        public bool ActionAvailable {
            get {
                var item = _inventoryManager.SelectedItem;
                if (item == null) {
                    return false;
                }

                var itemInfo = _itemsDic.FindInfo(item.itemId);
                return itemInfo.Price != 0;
            }
        }

        public void Interact() {
            if (!ActionAvailable) {
                return;
            }

            var item = _inventoryManager.SelectedItem;
            var itemInfo = _itemsDic.FindInfo(item.itemId);
            var coins = itemInfo.Price * item.count;

            if (_inventoryManager.TryConsume(item.itemId, item.count)) {

                if (coins < 500) {
                    _coinSpawner.SpawnCoins(coins);
                } else {
                    StartCoroutine(GiveALotOfCoins(coins));
                }
            }
        }

        private IEnumerator GiveALotOfCoins(int coins) {
            _audioSource.Play();
            yield return new WaitForSeconds(0.9f);
            _coinSpawner.SpawnCoins(coins);
        }
    }
}