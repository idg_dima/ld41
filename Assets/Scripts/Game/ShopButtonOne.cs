﻿using Game.Inventory;
using UnityEngine;
using Zenject;

namespace Game {
    public class ShopButtonOne : MonoBehaviour, IInteractiveObject {

        [SerializeField] private CoinSpawner _coinSpawner;

        private InventoryManager _inventoryManager;
        private ItemsDic _itemsDic;

        [Inject]
        public void Init(InventoryManager inventoryManager, ItemsDic itemsDic) {
            _inventoryManager = inventoryManager;
            _itemsDic = itemsDic;
        }

        public string ActionName {
            get {
                var item = _inventoryManager.SelectedItem;
                if (item == null) {
                    return "Select items in hotbar to sell them";
                }

                var itemInfo = _itemsDic.FindInfo(item.itemId);
                if (itemInfo.Price == 0) {
                    return string.Format("{0} can not be sold", itemInfo.Name);
                }

                var priceText = itemInfo.Price + (itemInfo.Price == 1 ? " coin" : " coins");
                return string.Format("Sell {0} for {1}", itemInfo.Name, priceText);
            }
        }

        public bool ActionAvailable {
            get {
                var item = _inventoryManager.SelectedItem;
                if (item == null) {
                    return false;
                }

                var itemInfo = _itemsDic.FindInfo(item.itemId);
                return itemInfo.Price != 0;
            }
        }

        public void Interact() {
            if (!ActionAvailable) {
                return;
            }

            var item = _inventoryManager.SelectedItem;
            var itemInfo = _itemsDic.FindInfo(item.itemId);
            var coins = itemInfo.Price;

            if (_inventoryManager.TryConsume(item.itemId)) {
                _coinSpawner.SpawnCoins(coins);
            }
        }
    }
}