﻿using UnityEngine;

namespace Game {

    [RequireComponent(typeof(MeshRenderer))]
    public class ShopScreen : MonoBehaviour {
        private Material _material;

        private void Awake() {
            _material = GetComponent<MeshRenderer>().material;
        }

        private void Update() {
            var progress = (Mathf.Sin(Time.time * 3) + 1) / 2f;
            var emission = Mathf.Lerp(1, 2, progress);
            var baseColor = Color.white;

            Color finalColor = baseColor * Mathf.LinearToGammaSpace(emission);
            _material.SetColor ("_EmissionColor", finalColor);
        }
    }
}