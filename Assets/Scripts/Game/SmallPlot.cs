﻿using System.Collections.Generic;
using Jir.Prefabs;
using UnityEngine;
using Zenject;

namespace Game {

    [RequireComponent(typeof(AudioSource))]
    public class SmallPlot : MonoBehaviour, IPlot {

        private Dictionary<int, Prefab<IPlant>> _plantPrefabsBySeedId;
        private IPlant _plant;
        private AudioSource _audioSource;

        [Inject]
        public void Init(Dictionary<int, Prefab<IPlant>> plantPrefabsBySeedId) {
            _plantPrefabsBySeedId = plantPrefabsBySeedId;
        }

        private void Awake() {
            _audioSource = GetComponent<AudioSource>();
        }

        public void PlantSeed(int seedId) {
            if (_plant != null) {
                return;
            }

            var prefab = _plantPrefabsBySeedId[seedId];
            _plant = prefab.Instantiate();

            _plant.ResetPosition(transform);
            _plant.ParentPlot = this;
            _plant.StartGrowingAnimation();
        }

        public void Fertilize() {
            if (_plant != null) {
                _plant.Fertilize();
            }
        }

        public void Clear() {
            _audioSource.Play();
            _plant = null;
        }
    }
}