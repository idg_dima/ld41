﻿using System;
using UnityEngine;

namespace Game.Tools {

    [RequireComponent(typeof(Animation))]
    [RequireComponent(typeof(AudioSource))]
    public class EquippedKatana : MonoBehaviour {
        private Animation _animation;
        private Action _hitCallback;
        private AudioSource _audioSource;

        private void Awake() {
            _animation = GetComponent<Animation>();
            _audioSource = GetComponent<AudioSource>();
        }

        public void PlaySwingAnimation(Action hitCallback) {
            _audioSource.Play();

            _animation.Stop();
            _animation.Play();
            _hitCallback = hitCallback;
        }

        private void OnHit() {
            if (_hitCallback != null) {
                _hitCallback();
            }
        }
    }
}