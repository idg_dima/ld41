﻿using Game.Inventory;
using UnityEngine;
using Zenject;

namespace Game.Tools {

    [RequireComponent(typeof(Animation))]
    [RequireComponent(typeof(AudioSource))]
    public class EquippedSeedGun : MonoBehaviour {
        [SerializeField] private ParticleSystem _particleSystem;
        [SerializeField] private MeshRenderer _screenRenderer;
        [SerializeField] private AudioClip _shotSound;
        [SerializeField] private AudioClip _noAmmoSound;
        [SerializeField] private AudioClip _changeSeedSound;

        private ItemsDic _itemsDic;
        private SeedGun _seedGun;
        private Animation _animation;
        private AudioSource _audioSource;

        [Inject]
        public void Init(ItemsDic itemsDic, SeedGun seedGun) {
            _itemsDic = itemsDic;
            _seedGun = seedGun;
        }

        private void Awake() {
            _animation = GetComponent<Animation>();
            _audioSource = GetComponent<AudioSource>();

            DisplaySeed(_seedGun.SelectedSeedId, false);
        }

        public void PlayShootAnimation(Vector3 hitPoint) {
            _audioSource.clip = _shotSound;
            _audioSource.Play();

            _animation.Stop();
            _animation.Play();

            _particleSystem.transform.LookAt(hitPoint);
            _particleSystem.Emit(100);
        }

        public void PlayNoAmmoAnimation() {
            _audioSource.clip = _noAmmoSound;
            _audioSource.Play();
        }

        public void DisplaySeed(int seedId, bool playSound) {
            if (playSound) {
                _audioSource.clip = _changeSeedSound;
                _audioSource.Play();
            }

            var itemInfo = _itemsDic.FindInfo(seedId);
            _screenRenderer.material.mainTexture = itemInfo.InventorySprite.texture;
        }
    }
}