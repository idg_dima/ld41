﻿using UnityEngine;

namespace Game.Tools {

    [RequireComponent(typeof(Animation))]
    [RequireComponent(typeof(AudioSource))]
    public class EquippedShotgun : MonoBehaviour {
        [SerializeField] private ParticleSystem _particleSystem;

        private Animation _animation;
        private AudioSource _audioSource;

        private void Awake() {
            _animation = GetComponent<Animation>();

            _audioSource = GetComponent<AudioSource>();
        }

        public void PlayShootAnimation(Vector3[] hitPoints) {
            _audioSource.Play();

            _animation.Stop();
            _animation.Play();

            for (var i = 0; i < hitPoints.Length; i++) {
                _particleSystem.transform.LookAt(hitPoints[i]);
                var shape = _particleSystem.shape;
                var distance = Mathf.Min(10, (hitPoints[i] - _particleSystem.transform.position).magnitude);
                shape.radius = distance / 2;
                shape.position = new Vector3(0, 0, distance / 2);
                _particleSystem.Emit(Mathf.FloorToInt(distance * 7.5f));
            }
        }
    }
}