﻿using UnityEngine;

namespace Game.Tools {
    public interface ITool {
        void Use(GameObject equippedObject, bool hit, RaycastHit hitInfo);

        void ChangeMode(GameObject equippedObject);
    }
}