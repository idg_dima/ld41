﻿using UnityEngine;
using Zenject;

namespace Game.Tools {
    public class Katana : ITool {
        private const int Damage = 1;
        private const float EffectiveDistance = 2f;
        private const float CooldownDuration = 0.4f;

        private readonly Camera _camera;
        private float _lastUseTime;
        private int _interactiveLayerMask;

        [Inject]
        public Katana(Camera camera) {
            _camera = camera;

            _interactiveLayerMask = LayerMask.GetMask("Interactive");
        }

        public void Use(GameObject equippedObject, bool hit, RaycastHit hitInfo) {
            if (_lastUseTime + CooldownDuration > Time.time) {
                return;
            }

            _lastUseTime = Time.time;
            var equippedKatana = equippedObject.GetComponent<EquippedKatana>();
            equippedKatana.PlaySwingAnimation(TryHit);
        }

        private void TryHit() {
            RaycastHit hitInfo;
            var ray = _camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));
            var hit = Physics.Raycast(ray, out hitInfo, 200, _interactiveLayerMask);

            if (hit && hitInfo.distance < EffectiveDistance) {
                var target = hitInfo.collider.gameObject;
                var destroyableTarget = target.GetComponent<IDestroyable>();
                if (destroyableTarget != null) {
                    destroyableTarget.TakeDamage(Damage);
                }
            }
        }

        public void ChangeMode(GameObject equippedObject) {
        }
    }
}