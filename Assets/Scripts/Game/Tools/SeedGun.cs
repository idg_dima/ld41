﻿using Game.Inventory;
using UnityEngine;
using Zenject;

namespace Game.Tools {
    public class SeedGun : ITool {
        private static readonly int[] PossibleSeedTypes = {
            ItemsDic.IdBerriesSeed, ItemsDic.IdCornSeed
        };

        private readonly InventoryManager _inventoryManager;
        private readonly Camera _camera;

        private int _seedIndex;

        [Inject]
        public SeedGun(InventoryManager inventoryManager, Camera camera) {
            _inventoryManager = inventoryManager;
            _camera = camera;
        }

        public void Use(GameObject equippedObject, bool hit, RaycastHit hitInfo) {
            var hadSeed = _inventoryManager.TryConsume(PossibleSeedTypes[_seedIndex]);
            if (!hadSeed) {
                if (equippedObject != null) {
                    var equippedGun = equippedObject.GetComponent<EquippedSeedGun>();
                    equippedGun.PlayNoAmmoAnimation();
                }

                return;
            }

            if (equippedObject != null) {
                var equippedGun = equippedObject.GetComponent<EquippedSeedGun>();
                var hitPoint = hit ? hitInfo.point : _camera.transform.forward * 200;
                equippedGun.PlayShootAnimation(hitPoint);
            }

            if (!hit) {
                return;
            }

            var target = hitInfo.collider.gameObject;
            var targetPlot = target.GetComponent<IPlot>();
            if (targetPlot != null) {
                targetPlot.PlantSeed(PossibleSeedTypes[_seedIndex]);
            }
        }

        public void ChangeMode(GameObject equippedObject) {
            _seedIndex = FindNextSeedIndex();
            var equippedGun = equippedObject.GetComponent<EquippedSeedGun>();
            equippedGun.DisplaySeed(PossibleSeedTypes[_seedIndex], true);
        }

        private int FindNextSeedIndex() {
            for (var i = _seedIndex + 1; i <= _seedIndex + PossibleSeedTypes.Length; i++) {
                var index = i % PossibleSeedTypes.Length;
                if (_inventoryManager.HasItem(PossibleSeedTypes[index])) {
                    return index;
                }
            }

            return 0;
        }

        public int SelectedSeedId {
            get {
                return PossibleSeedTypes[_seedIndex];
            }
        }
    }
}