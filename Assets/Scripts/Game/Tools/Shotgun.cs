﻿using UnityEngine;
using Zenject;

namespace Game.Tools {
    public class Shotgun : ITool {
        private const float CooldownDuration = 0.5f;
        private const float Spread = 0.1f;
        private const int Bullets = 8;

        private readonly Camera _camera;
        private readonly int _interactiveLayerMask;

        private float _lastUseTime;

        [Inject]
        public Shotgun(Camera camera) {
            _camera = camera;
            _interactiveLayerMask = LayerMask.GetMask("Interactive");
        }

        public void Use(GameObject equippedObject, bool hit, RaycastHit hitInfo) {
            if (_lastUseTime + CooldownDuration > Time.time) {
                return;
            }

            _lastUseTime = Time.time;

            var hitPoints = new Vector3[Bullets];
            for (var i = 0; i < Bullets; i++) {
                RaycastHit hitInfo2;
                var direction = _camera.transform.forward + Random.insideUnitSphere * Spread;
                var ray = new Ray(_camera.transform.position, direction);
                var hit2 = Physics.Raycast(ray, out hitInfo2, 200, _interactiveLayerMask);
                hitPoints[i] = hit2 ? hitInfo2.point : direction * 100;

                if (hit2) {
                    var plant = hitInfo2.collider.gameObject.GetComponent<IPlant>();
                    if (plant != null) {
                        plant.Fertilize();
                    } else {
                        var plot = hitInfo2.collider.gameObject.GetComponent<IPlot>();
                        if (plot != null) {
                            plot.Fertilize();
                        }
                    }
                }
            }

            if (equippedObject != null) {
                var equippedGun = equippedObject.GetComponent<EquippedShotgun>();
                equippedGun.PlayShootAnimation(hitPoints);
            }
        }

        public void ChangeMode(GameObject equippedObject) {
        }
    }
}