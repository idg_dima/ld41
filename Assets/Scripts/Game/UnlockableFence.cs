﻿using Game.Inventory;
using UnityEngine;
using Zenject;

namespace Game {

    [RequireComponent(typeof(AudioSource))]
    public class UnlockableFence : MonoBehaviour, IInteractiveObject {
        [SerializeField] private int _unlockPrice;
        [SerializeField] private GameObject _fenceGeometry;

        private InventoryManager _inventoryManager;
        private AudioSource _audioSource;

        [Inject]
        public void Init(InventoryManager inventoryManager) {
            _inventoryManager = inventoryManager;
        }

        private void Awake() {
            _audioSource = GetComponent<AudioSource>();
        }

        public string ActionName {
            get {
                if (ActionAvailable) {
                    return string.Format("Unlock this area ({0} Coins)", _unlockPrice);
                }
                return string.Format("You can unlock this area for {0} Coins", _unlockPrice);
            }
        }

        public bool ActionAvailable {
            get {
                return _inventoryManager.Coins >= _unlockPrice;
            }
        }

        public void Interact() {
            if (!ActionAvailable) {
                return;
            }

            _audioSource.Play();

            _fenceGeometry.SetActive(false);
            GetComponent<BoxCollider>().enabled = false;
            Destroy(gameObject, 1.5f);

            _inventoryManager.Coins -= _unlockPrice;
        }
    }
}