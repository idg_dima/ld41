﻿namespace Jir.Prefabs {
    public interface IPrefab {
        object InstantiateObject();
    }
}